DROP DATABASE IF EXISTS pizza4us;
CREATE DATABASE IF NOT EXISTS pizza4us;

DROP USER if EXISTS 'pizza4us'@'localhost';
GRANT ALL PRIVILEGES ON pizza4us.* TO 'pizza4us'@'localhost' IDENTIFIED BY 'pizza4us';

USE pizza4us;

CREATE TABLE `client` (
  `email_id` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`email_id`)
);

CREATE TABLE `address` (
  `address_id`       INT(11)      NOT NULL AUTO_INCREMENT,
  `email_id`         VARCHAR(100) NOT NULL,
  `name`             VARCHAR(100) NOT NULL,
  `phone`            VARCHAR(20)  NOT NULL,
  `street`           VARCHAR(100) NOT NULL,
  `city`             VARCHAR(100) NOT NULL,
  `province`         VARCHAR(2)   NOT NULL,
  `zip_code`         VARCHAR(7)   NOT NULL,
  `apartment_number` VARCHAR(10)  NULL,
  PRIMARY KEY (`address_id`),
  FOREIGN KEY (`email_id`) REFERENCES client (`email_id`)
);

CREATE TABLE `orders` (
  `order_id`   INT(11)      NOT NULL AUTO_INCREMENT,
  `email_id`   VARCHAR(100) NOT NULL,
  `address_id` INT          NOT NULL,
  `order_date` DATETIME     NOT NULL,
  PRIMARY KEY (`order_id`),
  FOREIGN KEY (`email_id`) REFERENCES client (`email_id`),
  FOREIGN KEY (`address_id`) REFERENCES address (`address_id`)
);

CREATE TABLE `size_pizza` (
  `size_id`     INT(11)      NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`size_id`)
);

CREATE TABLE `sauce` (
  `sauce_id`    INT(11)      NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`sauce_id`)
);

CREATE TABLE `cheese_type` (
  `cheese_id`   INT(11)      NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`cheese_id`)
);

CREATE TABLE `topping` (
  `topping_id`  INT(11)      NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`topping_id`)
);

CREATE TABLE `dough` (
  `dough_id`    INT(11)      NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`dough_id`)
);

CREATE TABLE `menu_item_type` (
  `menu_item_type_id`   INT(11)      NOT NULL AUTO_INCREMENT,
  `menu_item_type_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`menu_item_type_id`)
);

CREATE TABLE `menu_item` (
  `menu_item_id`          INT(11)      NOT NULL AUTO_INCREMENT,
  `menu_item_name`        VARCHAR(100) NOT NULL,
  `menu_item_description` VARCHAR(255) NULL,
  `menu_item_img`         VARCHAR(100) NULL,
  `menu_item_type_id`     INT          NOT NULL,
  PRIMARY KEY (`menu_item_id`),
  FOREIGN KEY (`menu_item_type_id`) REFERENCES menu_item_type (`menu_item_type_id`)
);

# Sauce, size, cheese and dough can be NULL to allow non-pizza items to be added to the order
CREATE TABLE `order_item` (
  `order_item_id` INT(11) NOT NULL AUTO_INCREMENT,
  `menu_item_id`  INT     NOT NULL,
  `order_id`      INT     NOT NULL,
  `sauce_id`      INT     NULL,
  `size_id`       INT     NULL,
  `cheese_id`     INT     NULL,
  `dough_id`      INT     NULL,
  PRIMARY KEY (`order_item_id`),
  FOREIGN KEY (`menu_item_id`) REFERENCES menu_item (`menu_item_id`),
  FOREIGN KEY (`order_id`) REFERENCES orders (`order_id`),
  FOREIGN KEY (`sauce_id`) REFERENCES sauce (`sauce_id`),
  FOREIGN KEY (`cheese_id`) REFERENCES cheese_type (`cheese_id`),
  FOREIGN KEY (`dough_id`) REFERENCES dough (`dough_id`)
);

CREATE TABLE `order_item_topping` (
  `order_item_topping_id` INT(11) NOT NULL AUTO_INCREMENT,
  `order_item_id`         INT     NOT NULL,
  `topping_id`            INT     NOT NULL,
  PRIMARY KEY (`order_item_topping_id`),
  FOREIGN KEY (`order_item_id`) REFERENCES order_item (`order_item_id`),
  FOREIGN KEY (`topping_id`) REFERENCES topping (`topping_id`)
);



# INSERT test user data, password = 1234
INSERT INTO client (email_id, password)
  VALUES ('tester@test.ca', '$2y$12$kWmAWFB0wUZw/W8VnNUB7.JYgBtjjT8VdJSns3Tkktd0Iu9WWaXCS');
INSERT INTO address (email_id, name, phone, street, city, province, zip_code)
  VALUES ('tester@test.ca', 'Jo Tester', '495 115-73-89', '37 Testing str.', 'City Seventeen', 'CA', 'N5Y3E6');
INSERT INTO address (email_id, name, phone, street, city, province, zip_code, apartment_number)
VALUES ('tester@test.ca', 'Joanne Tester', '495 115-73-89', '614 2nd Testing str.', 'City Seventeen', 'CA', 'N5Y3E6', '134');

# INSERT pizza sizes
INSERT INTO size_pizza (description) VALUES ('Small');
INSERT INTO size_pizza (description) VALUES ('Medium');
INSERT INTO size_pizza (description) VALUES ('Large');

# INSERT pizza dough types
INSERT INTO dough (description) VALUES ('Thin');
INSERT INTO dough (description) VALUES ('Regular');
INSERT INTO dough (description) VALUES ('Thick');

#INSERT pizza sauce types
INSERT INTO sauce (description) VALUES ('Mild');
INSERT INTO sauce (description) VALUES ('Bland');
INSERT INTO sauce (description) VALUES ('Hot');

# INSERT pizza cheese types
INSERT INTO cheese_type (description) VALUES ('Mozzarella');
INSERT INTO cheese_type (description) VALUES ('Cheddar');
INSERT INTO cheese_type (description) VALUES ('Parmesan');

# INSERT toppings
INSERT INTO topping (description) VALUES ('Broccoli');
INSERT INTO topping (description) VALUES ('Tuna');
INSERT INTO topping (description) VALUES ('Cat food');
INSERT INTO topping (description) VALUES ('Bacon');
INSERT INTO topping (description) VALUES ('Shrimps');
INSERT INTO topping (description) VALUES ('Beef');
INSERT INTO topping (description) VALUES ('Turkey');
INSERT INTO topping (description) VALUES ('Chicken');
INSERT INTO topping (description) VALUES ('Pineapple');
INSERT INTO topping (description) VALUES ('Cauliflower');

# INSERT pizza data
INSERT INTO menu_item_type (menu_item_type_name) VALUES ('pizza');

INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Pizza Uno',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'pizza1.jpeg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'pizza')
  );
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Pizza Duo',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'pizza2.jpeg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'pizza')
  );
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Pizza Tres',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'pizza3.jpeg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'pizza')
  );
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Pizza Quatro',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'pizza4.jpeg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'pizza')
  );
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Pizza Cinque',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'pizza5.jpeg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'pizza')
  );


# INSERT snacks data
INSERT INTO menu_item_type (menu_item_type_name) VALUES ('snacks');

INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Snack Uno',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'snack1.jpg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'snacks')
  );
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Snack Duo',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'snack2.jpg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'snacks')
  );
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
  VALUES ('Snack Tres',
          'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
          'snack3.jpg',
          (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'snacks')
  );



# INSERT drinks data
INSERT INTO menu_item_type (menu_item_type_name) VALUES ('drinks');

INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
VALUES ('Drink Uno',
        'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
        'drink1.jpg',
        (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'drinks')
);
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
VALUES ('Drink Duo',
        'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
        'drink2.jpg',
        (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'drinks')
);
INSERT INTO menu_item (menu_item_name, menu_item_description, menu_item_img, menu_item_type_id)
VALUES ('Drink Tres',
        'Some quick example text to build on the card title and make up the bulk of the card\'s content.',
        'drink3.jpg',
        (SELECT (menu_item_type_id) FROM menu_item_type WHERE menu_item_type_name = 'drinks')
);