// Contains custom js code

const ENDPOINTS_PATH = './../includes/endpoints/';
let toppingsNum = 6;

$(document).ready(() => {

  // Show modal-welcome
  $('#modalCookie').modal();


  // Event listener to swap signup/login modal windows
  $('.swap-modal').each(function() {
    "use strict";
    $(this).click(swapModals);
  });


  // Forms event listeners

  // Sends signup data to backend
  $('#modal-signup').submit(function(event) {
    "use strict";
    $.post(ENDPOINTS_PATH + 'signup.php', $(this).serialize(), User.signup);
    event.preventDefault();
  });

  // Sends login data to backend
  $('#modal-login').submit(function(event) {
    "use strict";
    $.post(ENDPOINTS_PATH + 'login.php', $(this).serialize(), User.login);
    event.preventDefault();
  });

  // Sends address data to backend
  $('#modal-address').submit(function(event) {
    "use strict";
    $.post(ENDPOINTS_PATH + 'address.php', $(this).serialize(), User.saveAddress);
    event.preventDefault();
  });

  // Complete order
  $('#btn-cart-submit').click(function(event) {
    "use strict";

    // if cart has items
    if (JSON.parse(sessionStorage.getItem('cart-array'))) {

      let items = JSON.parse(sessionStorage.getItem('cart-array'));
      let addressID = Number(sessionStorage.getItem('userAddressID'));
      // Send cart data to backend
      $.post(ENDPOINTS_PATH + 'placeOrder.php', {items: items, addressID: addressID}, User.orderPlaced);
    }

    event.preventDefault();
  });

  // Cancel Order, clear out cart
  $('#btn-cart-cancel').click(function(event) {
    "use strict";

    $('#table-cart tbody').html('');
    sessionStorage.removeItem('cart-array');
    sessionStorage.removeItem('cart-array-text');
    event.preventDefault();
  });

  // Adds new regular item to cart
  $('#modal-item-add2cart').click(function(event) {
    "use strict";

    let itemID = $('#modal-item-add2cart').attr('data-item-id');

    let cartArrayIDs = JSON.parse(sessionStorage.getItem('cart-array')) || [];
    cartArrayIDs.push({
      type: 'other',
      itemID: itemID
    });
    sessionStorage.setItem('cart-array', JSON.stringify(cartArrayIDs));

    // add row to cart table
    $('#table-cart tbody').append(`<tr>
              <td>${$('#modal-item-name').text()}</td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
            </tr>`);

    let cartArrayText = JSON.parse(sessionStorage.getItem('cart-array-text')) || [];

    // store text data to be able to re-render the cart contents on page reload
    cartArrayText.push(`<tr>
              <td>${$('#modal-item-name').text()}</td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
            </tr>`);
    sessionStorage.setItem('cart-array-text', JSON.stringify(cartArrayText));

    // hide item modal, show cart modal
    $('#modal-item').modal('hide');
    setTimeout(() => {
      $('#modalCart').modal('show');
    }, 500);
    event.preventDefault();
  });

  // Adds new pizza item to cart
  $('#form-item-pizza').submit(function(event) {
    "use strict";
    let pizzaSize = $('#pizza-size option:selected');
    let pizzaDough = $('#pizza-dough option:selected');
    let pizzaSauce = $('#pizza-sauce option:selected');
    let pizzaCheese = $('#pizza-cheese option:selected');
    let toppingsArray = [];
    let itemID = $('#modal-pizza-add2cart').attr('data-item-id');

    // IF all selects have values, add to cart
    if (pizzaSize.val() && pizzaDough.val() && pizzaSauce.val() && pizzaCheese.val()) {
      $('input[name="toppings"]:checked').each(function() {
        toppingsArray.push($(this).val());
      });

      let cartArrayIDs = JSON.parse(sessionStorage.getItem('cart-array')) || [];
      cartArrayIDs.push({
        type: 'pizza',
        itemID: itemID,
        size: pizzaSize.val(),
        dough: pizzaDough.val(),
        sauce: pizzaSauce.val(),
        cheese: pizzaSauce.val(),
        toppings: toppingsArray
      });
      sessionStorage.setItem('cart-array', JSON.stringify(cartArrayIDs));

      // add row to cart table
      $('#table-cart tbody').append(`<tr>
              <td>${$('#modal-pizza-name').text()}</td>
              <td>${pizzaSize.text()}</td>
              <td>${pizzaDough.text()}</td>
              <td>${pizzaSauce.text()}</td>
              <td>${pizzaCheese.text()}</td>
              <td>${$('input[name="toppings"]:checked').length}</td>
            </tr>`);

      let cartArrayText = JSON.parse(sessionStorage.getItem('cart-array-text')) || [];

      // store text data to be able to re-render the cart contents on page reload
      cartArrayText.push(`<tr>
              <td>${$('#modal-pizza-name').text()}</td>
              <td>${pizzaSize.text()}</td>
              <td>${pizzaDough.text()}</td>
              <td>${pizzaSauce.text()}</td>
              <td>${pizzaCheese.text()}</td>
              <td>${$('input[name="toppings"]:checked').length}</td>
            </tr>`);
      sessionStorage.setItem('cart-array-text', JSON.stringify(cartArrayText));

      // hide item modal, show cart modal
      $('#modal-pizza').modal('hide');
      setTimeout(() => {
        $('#modalCart').modal('show');
      }, 500);
    }
    event.preventDefault();
  });


  // Address list event listener
  $('#sel-address').change(function() {
    "use strict";
    console.log('Address changed');
    sessionStorage.setItem('userAddressID', $(this).val());
    $('#modalAddress').modal('hide');
  });

  $('#pizza-size').change(function() {
    "use strict";
    let val = $(this).val();
    console.log('Pizza size changed', val);

    switch (val) {
      case '1':
        toppingsNum = 2;
        console.log('case 1');
        break;
      case '2':
        toppingsNum = 4;
        break;
      case '3':
        toppingsNum = 6;
        break;
    }
    $("input[name='toppings']").removeAttr("disabled");
    $("input[name='toppings']").each(function() {
      $(this).prop("checked", false);
    });
  });


  // Buttons eventListeners

  // Show address list modal window
  $('#btn-address-upd').click(function(event) {
    "use strict";
    $('#modalAddress').modal();
    // Reset address list
    $('#sel-address').html('<option value="" disabled selected>Choose your option</option>');

    User.getUserAddr(sessionStorage.getItem('userID'));
    event.preventDefault();
  });

  // Show cart modal
  $('#btn-cart').click(function(event) {
    "use strict";
    $('#modalCart').modal('show');
    event.preventDefault();
  });

  // Logout button, logs the user out
  $('#user-logout').click(function(event) {
    "use strict";
    $.post(ENDPOINTS_PATH + 'logout.php', {email: sessionStorage.getItem('userID')}, User.logout);
    event.preventDefault();
  });

  // Navbar buttons event listeners. On click open approptiate menu section
  $("header a[href='#menu-pizzas']").click(() => {
    "use strict";
    $('#collapseOne').collapse('show');
  });
  $("header a[href='#menu-snacks']").click(() => {
    "use strict";
    $('#collapseTwo').collapse('show');
  });
  $("header a[href='#menu-drinks']").click(() => {
    "use strict";
    $('#collapseThree').collapse('show');
  });


  // Topping checkboxes eventListener. Disables & enables chkboxes depending on number of chkboxes checked
  $('input[name="toppings"]').change(function() {
    "use strict";
    console.log('checkbox changed!');

    if ($('input[name="toppings"]:checked').length > toppingsNum) {
      $("input[name='toppings']:not(:checked)").attr("disabled", true);
    } else {
      $("input[name='toppings']").removeAttr("disabled");
    }
  });


  // If userID is set (user logged in), show user menu, hide login link
  if (sessionStorage.getItem('userID')) {
    $('#user-menu').parent().show();
    $('#user-menu').text(sessionStorage.getItem('userID'));
    $('#login-link').hide();
    User.getUserAddr(sessionStorage.getItem('userID'));

  // else show login link, hide user menu
  } else {
    $('#user-menu').parent().hide();
    $('#login-link').show();
  }

  // If there are items left in cart, render them
  if (sessionStorage.getItem('cart-array-text') && sessionStorage.getItem('cart-array')) {
    let cartArrayTexts = JSON.parse(sessionStorage.getItem('cart-array-text'));
    cartArrayTexts.forEach(function(line) {
      $('#table-cart tbody').append(line);
    });
  }

  // Copies menu-item data to item-modal, displays item-modal
  $('.btn-add-to-cart').click(function(event) {
    "use strict";

    const itemID = $(this).data('itemId');
    const itemName = $(this).siblings('.menu-item-name').text();
    const itemImg = $(this).parent().siblings('.view.overlay').children('img').attr('src');
    const itemDescr = $(this).siblings('.menu-item-description').text();
    const itemType = $(this).data('itemType');

    //If address is set and chosen, proceed to order
    if (sessionStorage.getItem('userID') && sessionStorage.getItem('userAddressID')) {

      // Show pizza modal if itemType is pizza
      if (itemType === 'pizza') {
        $('#modal-pizza-name').text(itemName);
        $('#modal-pizza-img').attr('src', itemImg);
        $('#modal-pizza-description').text(itemDescr);
        $('#modal-pizza-add2cart').attr('data-item-id', itemID);

        $('#modal-pizza').modal();

      // Show regular item modal if itemType is not pizza
      } else {
        $('#modal-item-name').text(itemName);
        $('#modal-item-img').attr('src', itemImg);
        $('#modal-item-description').text(itemDescr);
        $('#modal-item-add2cart').attr('data-item-id', itemID);

        $('#modal-item').modal();
      }

    // If logged in, but no address is chosen, show address modal instead
    } else if (sessionStorage.getItem('userID') && !sessionStorage.getItem('userAddressID')) {
      console.log('logged in, no address');
      $('#modalAddress').modal();

    // If not logged in, show login modal
    } else {
      console.log('not logged in');
      $('#modalLoginForm').modal();
    }

    event.preventDefault();
  });

}); // END of $(document).ready


// A collection of callbacks for user AJAX requests
const User = {

  orderPlaced(response) {
    console.log('Order placed: ', response);

    // If order successful, clear out cart, display order summary
    if (response.status === "OK" && response.code === 200) {
      $('#modalCart').modal('hide');
      setTimeout(() => {
        $('#modalSummary').modal('show');
      }, 500);

      // Clear out cart
      $('#table-cart tbody').html('');
      // Clear out summary table
      $('#table-summary tbody').html('');

      $('#summary-order-num').text(response.orderID);

      let deliveryTime = 0;

      if (sessionStorage.getItem('cart-array-text') && sessionStorage.getItem('cart-array')) {
        let cartArrayTexts = JSON.parse(sessionStorage.getItem('cart-array-text'));
        cartArrayTexts.forEach(function(line) {
          $('#table-summary tbody').append(line);
          deliveryTime += 5;
        });
      }

      $('#summary-delivery-time').text(deliveryTime);

      sessionStorage.removeItem('cart-array');
      sessionStorage.removeItem('cart-array-text');
    }
  },

  signup(response) {
    console.log('Signup response: ', response);

    if (response.status === "OK" && response.code === 200) {
      swapModals();
    }
  },

  logout(response) {
    console.log('Logout response: ', response);

    sessionStorage.clear();
    $('#user-menu').text('');
    $('#user-menu').parent().hide();
    $('#login-link').show();
  },

  saveAddress(response) {
    console.log('Save Address: ', response);
    // Reset address list
    $('#sel-address').html('<option value="" disabled selected>Choose your option</option>');

    User.getUserAddr(sessionStorage.getItem('userID'));

  },

  getUserAddr(userID) {
    "use strict";
    $.get(ENDPOINTS_PATH + 'address.php', {userID: userID}, User.renderAddresses);
  },

  renderAddresses(response) {
    "use strict";
    console.log('Got address list: ', response);

    response.data.forEach((el) => {
      $('#sel-address').append($('<option>', {
        value: el.address_id,
        text: `${el.name}, ${el.street}, ${el.city}, ${el.phone}`
      }));
    });
  },

  login(response) {
    console.log('Login response: ', response);

    if (response.status === "OK" && response.code === 200) {

      console.log('Email: ', response.data.email);

      // Hide login form
      $('#modalLoginForm').modal('hide');
      // Hide login button
      $('#login-link').hide();
      // Save userID to session storage
      sessionStorage.setItem('userID', response.data.email);
      // Display user email in navbar
      $('#user-menu').text(sessionStorage.getItem('userID'));
      $('#user-menu').parent().show();

      // Reset address list
      $('#sel-address').html('<option value="" disabled selected>Choose your option</option>');
      // Get user addresses
      User.getUserAddr(sessionStorage.getItem('userID'));
      // Show address list modal window
      setTimeout(() => {
        "use strict";
        $('#modalAddress').modal('show');
      }, 500);
    }
  }
};






// Changes the .jumbo bg-image every 3 seconds
(function() {
  "use strict";

  const imageArray = [
    './img/jumbo2.jpeg',
    './img/jumbo3.jpeg',
    './img/jumbo1.jpeg',
  ];
  const arrayLength = imageArray.length;
  let index = 0;

  // Preload images to avoid img flickering on bg change
  imageArray.forEach((img) => {
    const image = new Image();
    image.src = img;
  });

  setInterval(() => {
    $('.jumbo').css('background-image', `url('${imageArray[index]}')`);
    index = (index + 1) % arrayLength;
  }, 3000);
})();


// Swaps Login/Register modals
function swapModals() {
  "use strict";

  if ($('#modalLoginForm').hasClass('show')) {
    $('#modalLoginForm').modal('hide');
    setTimeout(() => {
      $('#modalRegisterForm').modal('show');
    }, 500);
  }
  else if ($('#modalRegisterForm').hasClass('show')) {
    $('#modalRegisterForm').modal('hide');
    setTimeout(() => {
      $('#modalLoginForm').modal('show');
    }, 500);
  }
}
