<?php

require_once '../includes/config.php';

session_start();

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/mdb.min.css">
  <link rel="stylesheet" href="./css/style.css">
  <title>Pizza4Us</title>
</head>

<body>

<div id="wrap">

  <header class="fixed-top">
  <!-- Top-Nav -->
    <?php require_once(TEMPLATES_PATH . '/top-nav.php') ?>
  <!-- /Top-Nav -->
  </header>

  <main>
    <div class="container-fluid">
      <div class="row">
      <!-- Jumbotron -->
        <?php require_once(TEMPLATES_PATH . '/jumbotron.php') ?>
      <!-- /Jumbotron -->
      </div>
    </div>
    <div class="container">
    <!-- Accordion wrapper -->
      <?php require_once(TEMPLATES_PATH . '/menu-accordion.php') ?>
    <!-- /Accordion wrapper -->
    </div>
  </main>

  <!-- Footer -->
    <?php require_once(TEMPLATES_PATH . '/footer.php') ?>
  <!-- /Footer -->
</div>


<!-- Login MODAL -->
  <?php require_once(TEMPLATES_PATH . '/modals/modal-login.php') ?>
<!-- /Login MODAL-->

<!-- Sign Up MODAL -->
  <?php require_once(TEMPLATES_PATH . '/modals/modal-signup.php') ?>
<!-- /Sign Up MODAL -->

<!-- Welcome message MODAL -->
  <?php require_once(TEMPLATES_PATH . '/modals/modal-welcome.php') ?>
<!-- /Welcome message MODAL -->

<!-- Address MODAL -->
<?php require_once(TEMPLATES_PATH . '/modals/modal-address.php') ?>
<!-- /Address MODAL -->

<!-- Shopping Cart MODAL -->
<?php require_once(TEMPLATES_PATH . '/modals/modal-cart.php') ?>
<!-- /Shopping Cart MODAL -->

<!-- Add Item to Cart MODAL -->
<?php require_once(TEMPLATES_PATH . '/modals/modal-item.php') ?>
<!-- Add Item to Cart MODAL -->

<!-- Add Pizza to Cart MODAL -->
<?php require_once(TEMPLATES_PATH . '/modals/modal-item-pizza.php') ?>
<!-- Add Pizza to Cart MODAL -->

<!-- Order Summary MODAL -->
<?php require_once(TEMPLATES_PATH . '/modals/modal-order-placed.php') ?>
<!-- Order Summary MODAL -->

<!--SCRIPTS-->
<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="./js/popper.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/mdb.min.js"></script>
<script type="text/javascript" src="./js/index.js"></script>
</body>
</html>