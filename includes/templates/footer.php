<?php
// Markup for a component
?>

<footer class="page-footer font-small orange darken-4 pt-4 mt-0">
  <!--Footer Links-->
  <div class="container-fluid text-center text-md-left">
    <div class="row">

      <!--First column-->
      <div class="col-md-6">
        <h5 class="text-uppercase">Welcome to footer section!</h5>
        <p>Here you won't find anything useful!</p>
      </div>
      <!--/.First column-->

      <!--Second column-->
      <div class="col-md-6">
        <h5 class="text-uppercase">Social Links</h5>
        <ul class="list-unstyled social">
          <li class="list-inline-item px-sm-2 pl-md-0 pr-md-4">
            <a href="#!">
              <i class="fa fa-facebook-official" aria-hidden="true"></i>Facebook
            </a>
          </li>
          <li class="list-inline-item px-sm-2 pl-md-0 pr-md-4">
            <a href="#!">
              <i class="fa fa-google-plus-square" aria-hidden="true"></i>Google+
            </a>
          </li>
          <li class="list-inline-item px-sm-2 pl-md-0 pr-md-4">
            <a href="#!">
              <i class="fa fa-instagram" aria-hidden="true"></i>Instagram
            </a>
          </li>
          <li class="list-inline-item px-sm-2 pl-md-0 pr-md-4">
            <a href="#!">
              <i class="fa fa-pinterest-square" aria-hidden="true"></i>Pinterest
            </a>
          </li>
        </ul>
      </div>
      <!--/.Second column-->
    </div>
  </div>
  <!--/.Footer Links-->

  <!--Copyright-->
  <div class="footer-copyright pt-1 text-center">
    <div class="container-fluid">
      Developed by: <a href="#"> IWD-04 Group#4, 2018 </a>

    </div>
  </div>
  <!--/.Copyright-->
</footer>

