<?php

// Markup for a modal window component

  require_once(LIBRARY_PATH . '/db.php');

  $itemsArray = [
    'pizza' => getItems('pizza'),
    'snacks' => getItems('snacks'),
    'drinks' => getItems('drinks')
  ];

  // TODO: Move this into Items class
  function getItems($itemType) {
    global $db_conn;
    $returnArray = [];
    $qry = "SELECT menu_item_id, menu_item_name, menu_item_description, menu_item_img, menu_item_type_name
            FROM menu_item
            JOIN menu_item_type ON menu_item.menu_item_type_id = menu_item_type.menu_item_type_id
            WHERE menu_item_type_name = '{$itemType}'";
    $items = $db_conn->query($qry);
    while ($row = $items->fetch_array(MYSQLI_ASSOC)) { $returnArray[] = $row; }
    return $returnArray;
  }
?>

<div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">


  <!-- Pizzas card -->
  <div class="card bg-transparent" id="menu-pizzas">
    <!-- Card header -->
    <div class="card-header" role="tab" id="headingOne">
      <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <h5 class="mb-0 text-center">
          Pizzas Menu <i class="fa fa-angle-down rotate-icon font-weight-bold"></i>
        </h5>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseOne" class="collapse show row" role="tabpanel" aria-labelledby="headingOne"
         data-parent="#accordionEx" >
      <div class="mt-3 container">
        <div class="row d-flex justify-content-center">
          <?php foreach ( $itemsArray['pizza'] as $item ) { require(TEMPLATES_PATH . '/menu-item.php'); } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Pizzas card -->

  <!-- Snacks card -->
  <div class="card bg-transparent" id="menu-snacks">
    <!-- Card header -->
    <div class="card-header" role="tab" id="headingTwo">
      <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <h5 class="mb-0 text-center">
          Snacks Menu <i class="fa fa-angle-down rotate-icon font-weight-bold"></i>
        </h5>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx" >
      <div class="mt-3 container">
        <div class="row d-flex justify-content-center">
          <?php foreach ( $itemsArray['snacks'] as $item ) { require(TEMPLATES_PATH . '/menu-item.php'); } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Snacks card -->


  <!-- Drinks card -->
  <div class="card bg-transparent" id="menu-drinks">

    <!-- Card header -->
    <div class="card-header" role="tab" id="headingThree">
      <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        <h5 class="mb-0 text-center">
          Drinks Menu <i class="fa fa-angle-down rotate-icon font-weight-bold"></i>
        </h5>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordionEx">
      <div class="mt-3 container">
        <div class="row d-flex justify-content-center">
          <?php foreach ( $itemsArray['drinks'] as $item ) { require(TEMPLATES_PATH . '/menu-item.php'); } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- /.Drinks card -->

</div>