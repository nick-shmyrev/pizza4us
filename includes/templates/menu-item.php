<?php
// Markup for a modal window component
?>

<div class="card col-md-4">
  <!--Card image-->
  <div class="view overlay">
    <img src="<?php isset($item['menu_item_img']) ? print "./img/{$item['menu_item_img']}" : print ''  ?>" class="img-fluid">
    <a href="#">
      <div class="mask rgba-white-slight"></div>
    </a>
  </div>
  <!--Card content-->
  <div class="card-body">
    <!--Title-->
    <h4 class="card-title menu-item-name"><?php isset($item['menu_item_name']) ? print "{$item['menu_item_name']}" :
        print ''  ?></h4>
    <!--Text-->
    <p class="card-text menu-item-description"><?php isset($item['menu_item_description']) ? print "{$item['menu_item_description']}" : print '' ?></p>
    <a href="#" class="btn btn-primary d-block btn-add-to-cart"
       data-item-id="<?php isset($item['menu_item_id']) ? print "{$item['menu_item_id']}" : print '' ?>"
       data-item-type="<?php isset($item['menu_item_type_name']) ? print "{$item['menu_item_type_name']}" : print '' ?>">Add to cart</a>
  </div>
</div>