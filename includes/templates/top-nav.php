<?php
// Markup for a modal window component
?>

<nav class="navbar navbar-expand-sm navbar-dark orange darken-4">

  <!-- Navbar brand -->
  <a class="navbar-brand font-weight-bold mr-5" href="#">Pizza4Us</a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent">

    <!-- Links -->
    <ul class="navbar-nav w-100">
      <li class="nav-item">
        <a class="nav-link" href="#menu-pizzas">Pizzas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#menu-snacks">Snacks</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#menu-drinks">Drinks</a>
      </li>
      <li class="hidden-md-up"><hr class="white"></li>
      <li class="nav-item ml-sm-auto" id="login-link">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#modalLoginForm">
          Login / Register
          <i class="fa fa-sign-in ml-1" aria-hidden="true"></i>
        </a>
      </li>
      <!-- Dropdown -->
      <li class="nav-item dropdown ml-sm-auto">
        <a class="nav-link dropdown-toggle" id="user-menu" data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="false"></a>
        <div class="dropdown-menu dropdown-primary orange darken-4" aria-labelledby="user-menu">
          <a class="dropdown-item" href="#" id="btn-cart">See Cart</a>
          <a class="dropdown-item" href="#" id="btn-address-upd">Update address</a>
          <hr class="mx-2">
          <a class="dropdown-item" href="#" id="user-logout">Log out</a>
        </div>
      </li>
    </ul>
    <!-- /.Links -->


  </div>
  <!-- /.Collapsible content -->

</nav>