<?php
// Markup for a modal window component
?>

<div class="card card-image jumbo">
  <div class="text-white text-center d-flex align-items-center rgba-stylish-strong py-5 px-4">
    <div class="py-5">
      <!--Content-->
      <h2 class="card-title pt-3 mb-5 font-bold">Pizza 4 Us - Because reasons</h2>
      <p class="px-5 pb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat
        fugiat, laboriosam, voluptatem, optio vero odio nam sit officia
        accusamus minus error nisi architecto nulla ipsum dignissimos.
        Odit sed qui, dolorum!</p>
      <!--Content-->
    </div>
  </div>
</div>