<?php
// Markup for a modal window component
?>

<!-- Modal: modal-item -->
<div class="modal fade" id="modal-item" tabindex="-11" role="dialog" aria-labelledby="modal-item"
     aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-5">
            <img class="d-block w-100" id="modal-item-img" src="" alt="Menu Item Image">
          </div>
          <div class="col-lg-7">
            <h2 class="h2-responsive product-name">
              <strong id="modal-item-name">Product Name</strong>
            </h2>
            <h4 class="h4-responsive">
              <span class="green-text">
                <strong>$99</strong>
              </span>
              <span class="grey-text">
                <small>
                  <s>$89</s>
                </small>
              </span>
            </h4>

            <div id="modal-item-description">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
              non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
            </div>

            <!-- Add to Cart -->
            <div class="card-body">

              <div class="text-center">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" id="modal-item-add2cart">Add to cart
                  <i class="fa fa-cart-plus ml-2" aria-hidden="true"></i>
                </button>
              </div>
            </div>
            <!-- /.Add to Cart -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modal-item -->