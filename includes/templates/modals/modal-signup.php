<?php
// Markup for a modal window component
?>

<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <form id="modal-signup">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-bold">Sign up</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-3">

          <div class="md-form">
            <i class="fa fa-envelope prefix grey-text"></i>
            <input type="email" id="txt-signup-email" name="txt-signup-email" class="form-control validate" required
                   maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-signup-email">Your email</label>
          </div>

          <div class="md-form">
            <i class="fa fa-lock prefix grey-text"></i>
            <input type="password" id="txt-signup-pass" name="txt-signup-pass" class="form-control validate" required
                   minlength="4" maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-signup-pass">Your password</label>
          </div>

        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button class="btn btn-orange">Sign up</button>
        </div>
      </form>

      <div class="modal-footer border-top-0 d-flex justify-content-center">
        <p>Already have an account? <a href="#" class="swap-modal">Log In!</a></p>
      </div>

    </div>
  </div>
</div>