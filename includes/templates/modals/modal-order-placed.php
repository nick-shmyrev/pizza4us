<?php
// Markup for a modal window component
?>

<!-- Modal: modalCart -->
<div class="modal fade" id="modalSummary" tabindex="-16" role="dialog" aria-labelledby="modalSummary"
     aria-hidden="true">
  <div class="modal-dialog cart w-75" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Summary for Order <strong>#<span
              id="summary-order-num"></span></strong></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body">

        <table class="table table-hover" id="table-summary">
          <thead>
          <tr>
            <th>Product name</th>
            <th>Size</th>
            <th>Dough</th>
            <th>Sauce</th>
            <th>Cheese</th>
            <th>Number of Toppings</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <p>Estimated delivery time: <span id="summary-delivery-time">15</span> minute(s)</p>
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalCart -->