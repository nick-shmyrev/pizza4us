<?php
// Markup for a modal window component
?>

<!--Modal: modalCookie-->
<div class="modal fade top" id="modalCookie" tabindex="-12" role="dialog" aria-labelledby="modalCookie"
     aria-hidden="true"
     data-backdrop="false">
  <div class="modal-dialog modal-frame modal-bottom modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Body-->
      <div class="modal-body">
        <div class="row d-flex justify-content-center align-items-center">

          <p class="pt-3 pr-2">Welcome to pizza4us! Register before you try to order anything, or login if you
            already have an account with us. Also, we use cookies.</p>

          <a type="button" class="btn orange darken-4 text-white waves-effect" data-dismiss="modal">Ok, got
            it</a>
        </div>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalCookie-->