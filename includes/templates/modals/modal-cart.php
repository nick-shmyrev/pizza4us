<?php
// Markup for a modal window component
?>

<!-- Modal: modalCart -->
<div class="modal fade" id="modalCart" tabindex="-15" role="dialog" aria-labelledby="modalCart" aria-hidden="true">
  <div class="modal-dialog cart w-75" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Your cart</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body">

        <table class="table table-hover" id="table-cart">
          <thead>
          <tr>
            <th>Product name</th>
            <th>Size</th>
            <th>Dough</th>
            <th>Sauce</th>
            <th>Cheese</th>
            <th>Number of Toppings</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
        <button class="btn btn-danger" id="btn-cart-cancel">Cancel Order</button>
        <button class="btn btn-primary" id="btn-cart-submit">Checkout</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalCart -->