<?php
// Markup for a modal window component
?>

<div class="modal fade" id="modalAddress" tabindex="-13" role="dialog" aria-labelledby="modalAddress"
     aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">


        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-bold">Add/Change Address</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-3 mb-3">
          <div class="md-form">
            <label for="sel-address" class=" mb-5">Select Address:
              <select id="sel-address" class="w-100">
                <option value="" disabled selected>Choose your option</option>
              </select>
            </label>
          </div>
        </div>

      <form id="modal-address">
        <div class="modal-body mx-3 mb-5">
          <div class="md-form">
            <input type="text" id="txt-address-name" name="txt-address-name" class="form-control validate" required
                   minlength="2"
                   maxlength="100"
                   pattern="^[a-zA-Z ,.'-]+$">
            <label data-error="wrong" data-success="right" for="txt-address-name">Name</label>
          </div>
          <div class="md-form">
            <input type="tel" id="txt-address-phone" name="txt-address-phone" class="form-control validate" required
                   minlength="10" maxlength="20" pattern="^(((\([0-9]{3}\))|([0-9]{3})) |[0-9]{3}-)[0-9]{3}-[0-9]{2}(-|)[0-9]{2}$">
            <label data-error="wrong" data-success="right" for="txt-address-phone">Phone Number</label>
          </div>
          <div class="md-form">
            <input type="text" id="txt-address-street" name="txt-address-street" class="form-control validate" required
                   maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-address-street">Street</label>
          </div>

          <div class="md-form">
            <input type="text" id="txt-address-city" name="txt-address-city" class="form-control validate" required
                   minlength="4" maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-address-city">City</label>
          </div>

          <div class="md-form">
            <label for="sel-province" class="mb-5">Select Province:
              <select id="sel-province" name="sel-province" class="w-100">
                <option value="CA" selected>CA</option>
                <option value="AB">AB</option>
                <option value="BC">BC</option>
                <option value="MB">MB</option>
                <option value="NL">NL</option>
                <option value="NT">NT</option>
                <option value="NU">NU</option>
                <option value="PE">PE</option>
                <option value="QC">QC</option>
                <option value="SK">SK</option>
                <option value="YT">YT</option>
              </select>
            </label>
          </div>

        </div>
        <div class="modal-body mx-3 mt-0">
          <div class="md-form">
            <input type="text" id="txt-address-zip" name="txt-address-zip" class="form-control validate" required
                   minlength="4" maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-address-zip">Postal Code</label>
          </div>

          <div class="md-form">
            <input type="text" id="txt-address-apt" name="txt-address-apt" class="form-control validate"
                   minlength="1" maxlength="10">
            <label data-error="wrong" data-success="right" for="txt-address-apt">Apartment Number</label>
          </div>
        </div>

        <div class="modal-footer d-flex justify-content-center">
          <button class="btn btn-orange">Save Address</button>
        </div>
      </form>


    </div>
  </div>
</div>