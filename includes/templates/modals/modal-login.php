<?php
// Markup for a modal window component
?>

<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <form id="modal-login">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-bold">Log In</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-3">
          <div class="md-form">
            <i class="fa fa-envelope prefix grey-text"></i>
            <input type="email" id="txt-login-email" name="txt-login-email" class="form-control validate" required
                   maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-login-email">Your email</label>
          </div>

          <div class="md-form">
            <i class="fa fa-lock prefix grey-text"></i>
            <input type="password" id="txt-login-pass" name="txt-login-pass" class="form-control validate" required
                   minlength="4" maxlength="100">
            <label data-error="wrong" data-success="right" for="txt-login-pass">Your password</label>
          </div>

        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button class="btn btn-orange">Login</button>
        </div>
      </form>

      <div class="modal-footer border-top-0 d-flex justify-content-center">
        <p>Don't have an account yet? <a href="#" class="swap-modal">Sign Up!</a></p>
      </div>

    </div>
  </div>
</div>