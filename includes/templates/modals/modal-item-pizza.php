<?php

// Markup for a modal window component

require_once(LIBRARY_PATH . '/db.php');

$optionsArray = getItemOptions();

// TODO: Move this into Items class
function getItemOptions() {
  global $db_conn;
  $returnArray = [];

  $qry = "SELECT size_id, description FROM size_pizza";
  $result = $db_conn->query($qry);
  while ($row = $result->fetch_array(MYSQLI_ASSOC)) { $returnArray['size'][] = $row; }

  $qry = "SELECT dough_id, description FROM dough";
  $result = $db_conn->query($qry);
  while ($row = $result->fetch_array(MYSQLI_ASSOC)) { $returnArray['dough'][] = $row; }

  $qry = "SELECT sauce_id, description FROM sauce";
  $result = $db_conn->query($qry);
  while ($row = $result->fetch_array(MYSQLI_ASSOC)) { $returnArray['sauce'][] = $row; }

  $qry = "SELECT cheese_id, description FROM cheese_type";
  $result = $db_conn->query($qry);
  while ($row = $result->fetch_array(MYSQLI_ASSOC)) { $returnArray['cheese'][] = $row; }

  $qry = "SELECT topping_id, description FROM topping";
  $result = $db_conn->query($qry);
  while ($row = $result->fetch_array(MYSQLI_ASSOC)) { $returnArray['toppings'][] = $row; }

  return $returnArray;
}
?>

<!-- Modal: modalQuickView -->
<div class="modal fade" id="modal-pizza" tabindex="-10" role="dialog" aria-labelledby="modal-pizza"
     aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-5">
            <img class="d-block w-100" id="modal-pizza-img" src="" alt="Menu Item Image">
          </div>
          <div class="col-lg-7">
            <h2 class="h2-responsive product-name">
              <strong id="modal-pizza-name">Product Name</strong>
            </h2>
            <h4 class="h4-responsive">
              <span class="green-text">
                <strong>$99</strong>
              </span>
              <span class="grey-text">
                <small>
                  <s>$89</s>
                </small>
              </span>
            </h4>

            <div id="modal-pizza-description">
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
              non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
            </div>

            <!-- Add to Cart -->
            <form id="form-item-pizza">
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <label for="pizza-size" class="pizza-option-label mb-0 mt-2">Select Size:</label>
                    <select id="pizza-size" class="mdb-select colorful-select dropdown-primary">
                      <option value="" disabled selected>Choose your option</option>
                      <?php foreach ($optionsArray['size'] as $option) print "<option value='{$option['size_id']}'>{$option['description']}</option>" ?>
                    </select>
                  </div>

                  <div class="col-sm-6">
                    <label for="pizza-dough" class="pizza-option-label mb-0 mt-2">Select Dough:</label>
                    <select id="pizza-dough">
                      <option value="" disabled selected>Choose your option</option>
                      <?php foreach ($optionsArray['dough'] as $option) print "<option value='{$option['dough_id']}'>{$option['description']}</option>" ?>
                    </select>
                  </div>

                  <div class="col-sm-6">
                    <label for="pizza-sauce" class="pizza-option-label mb-0 mt-2">Select Sauce:</label>
                    <select id="pizza-sauce">
                      <option value="" disabled selected>Choose your option</option>
                      <?php foreach ($optionsArray['sauce'] as $option) print "<option value='{$option['sauce_id']}'>{$option['description']}</option>" ?>
                    </select>
                  </div>

                  <div class="col-sm-6">
                    <label for="pizza-cheese" class="pizza-option-label mb-0 mt-2">Select Cheese:</label>
                    <select id="pizza-cheese" class="mdb-select colorful-select dropdown-primary">
                      <option value="" disabled selected>Choose your option</option>
                      <?php foreach ($optionsArray['cheese'] as $option) print "<option value='{$option['cheese_id']}'>{$option['description']}</option>" ?>
                    </select>
                  </div>

                  <div class="col-sm-12 mt-3 d-flex justify-content-around flex-wrap">
                    <?php foreach ($optionsArray['toppings'] as $topping) {
                      print "<label><input type='checkbox' name='toppings' value='{$topping['topping_id']}'> {$topping['description']}</label>";
                    } ?>
                  </div>

                </div>

                <div class="text-center mt-2">

                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button class="btn btn-primary" id="modal-pizza-add2cart">Add to cart
                    <i class="fa fa-cart-plus ml-2" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            </form>

            <!-- /.Add to Cart -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalQuickView -->