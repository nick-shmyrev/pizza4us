<?php

// This file is called by front-end to register a new user

require_once('../config.php');
require_once(LIBRARY_PATH . '/db.php');
require_once(LIBRARY_PATH . '/formValidations.php');


header("Content-Type: application/json");

$response = [];

// Refuse GET requests
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $response = [
    'status' => 'Method Not Allowed',
    'code' => 405
  ];
}

// If called with post request, sanitize and validate data, create a new user
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  $email = $db_conn->real_escape_string(trim(strtolower($_POST['txt-signup-email'])));
  $password = trim($_POST['txt-signup-pass']); /* Password will be hashed before saving, so no need to sanitize */

  // Proceed to checking if user already exists if data passes validation
  if (signupDataValid($email, $password)) {

    // Will return 1 if email exists, or 0 if it doesn't
    $emailExists = $db_conn->query("SELECT email_id FROM client WHERE email_id = '{$email}'")->num_rows;

    // If email is not taken, proceed to save new user
    if (!$emailExists) {

      // Hash password
      $hash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);

      // Save data to DB
      $qry = "INSERT INTO client (email_id, password) VALUES ('{$email}', '{$hash}')";
      $db_conn->query($qry);

      // Return 200 OK
      $response = [
        'status' => 'OK',
        'code' => 200
      ];

    // Return 409 Conflict if email already exists
    } else {
      $response = [
        'status' => 'Conflict',
        'code' => 409
      ];
    }

  // Return 400 Bad Request if validation fails
  } else {
    $response = [
      'status' => 'Bad Request',
      'code' => 400
    ];
  }
}

$db_conn->close();

// Return a server response
print json_encode($response);
