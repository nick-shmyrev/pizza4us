<?php

// This file is called by front-end to login an existing user

session_start();

require_once('../config.php');
require_once(LIBRARY_PATH . '/db.php');
require_once(LIBRARY_PATH . '/formValidations.php');


header("Content-Type: application/json");

$response = [];

// Refuse GET requests
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $response = [
    'status' => 'Method Not Allowed',
    'code' => 405
  ];
}

// If called with post request, sanitize & validate data, check password and login user
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

  $email = $db_conn->real_escape_string(trim(strtolower($_POST['txt-login-email'])));
  $password = trim($_POST['txt-login-pass']); /* Password will be hashed before saving, so no need to sanitize */

  // Proceed to checking if user exists if data passes validation
  if (loginDataValid($email)) {

    // Will return 1 if email was found, or 0 if it wasn't
    $userExists = $db_conn->query("SELECT email_id FROM client WHERE email_id = '{$email}'")->num_rows;

    // If user exists, proceed to check password
    if ($userExists) {

      $rs = $db_conn->query("SELECT email_id, password FROM client WHERE email_id = '{$email}'")->fetch_assoc();

      $passwordMatches = password_verify($password, $rs['password']);

      // If password matches, return 200 OK, name & email
      if ($passwordMatches) {

        $_SESSION['client_email_id'] = $rs['email_id'];
        $_SESSION['last_login'] = time();

        $response = [
          'status' => 'OK',
          'code' => 200,
          'message' => "Logged In",
          'data' => [
            'email' => "{$_SESSION['client_email_id']}"
          ]
        ];

      // If password doesn't match, return 401 Unauthorized
      } else {
        $response = [
          'status' => 'Unauthorized',
          'code' => 401,
          'message' => "Login and/or password did not match"
        ];
      }

    // if user not found return 401 Unauthorized
    } else {
      $response = [
        'status' => 'Unauthorized',
        'code' => 401,
        'message' => "Login and/or password did not match"
      ];
    }

  // Return 400 Bad Request if invalid email was given
  } else {
    $response = [
      'status' => 'Bad Request',
      'code' => 400,
      'message' => "Email format is invalid"
    ];
  }
}

$db_conn->close();

print json_encode($response);