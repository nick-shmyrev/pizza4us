<?php

// This file is called by front-end to log out a user

session_start();

require_once('../config.php');

header("Content-Type: application/json");

$response = [];

// Refuse GET requests
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $response = [
    'status' => 'Method Not Allowed',
    'code' => 405
  ];
}

// If called with post request, logout user
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $email = $_POST['email'];

  $response = [
    'status' => 'OK',
    'code' => 200,
    'message' => 'Logged Out',
    'data' => [
      'email' => "{$_SESSION['client_email_id']}"
    ]
  ];

  unset($_SESSION['client_email_id']);
  unset($_SESSION['last_login']);
}

print json_encode($response);