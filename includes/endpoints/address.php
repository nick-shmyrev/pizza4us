<?php

// This file is called by front-end to save a new client address, or retrieve the list of existing addresses

session_start();

require_once('../config.php');
require_once(LIBRARY_PATH . '/db.php');
require_once(LIBRARY_PATH . '/formValidations.php');


header("Content-Type: application/json");

$response = [];

// If called by a get request, respond with a list of existing addresses
if ($_SERVER['REQUEST_METHOD'] === 'GET'
    && isset($_SESSION['client_email_id'])
    && $_SESSION['client_email_id'] === $_GET['userID']) {

  $qry = "SELECT address_id, name, phone, street, city, province, zip_code, apartment_number FROM address 
    WHERE email_id = '{$_SESSION['client_email_id']}'";
  $result = $db_conn->query($qry);

  $response = [
    'status' => 'OK',
    'code' => 200,
    'data' => []
  ];

  while ($row = $result->fetch_assoc()) {
    $response['data'][] = $row;
  }

} else {
  $response = [
    'status' => 'Bad Request',
    'code' => 400
  ];
}

// If called with a post request, sanitize & validate incoming data and save a new address
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['client_email_id'])) {

  $name = $db_conn->real_escape_string(trim($_POST['txt-address-name']));
  $phone = $db_conn->real_escape_string(trim($_POST['txt-address-phone']));
  $street = $db_conn->real_escape_string(trim($_POST['txt-address-street']));
  $city = $db_conn->real_escape_string(trim($_POST['txt-address-city']));
  $province = $db_conn->real_escape_string($_POST['sel-province']);
  $zipcode = $db_conn->real_escape_string($_POST['txt-address-zip']);
  $aptNum = isset($_POST['txt-address-apt']) ? $db_conn->real_escape_string($_POST['txt-address-apt']) : null;


  // If data is valid, save address
  if (addressValid($name, $phone, $street, $city, $province, $zipcode, $aptNum)) {

    $qry = "INSERT INTO address (email_id, name, phone, street, city, province, zip_code, apartment_number) 
      VALUES ('{$_SESSION['client_email_id']}', '{$name}', '{$phone}', '{$street}', '{$city}', '{$province}', '{$zipcode}', '{$aptNum}')";
    $db_conn->query($qry);

    $response = [
      'status' => 'OK',
      'code' => 200
    ];

    // Return 400 Bad Request if validation fails
  } else {
    $response = [
      'status' => 'Bad Request',
      'code' => 400
    ];
  }
} else if (!isset($_SESSION['client_email_id'])) {
  $response = [
    'status' => 'Unauthorized',
    'code' => 403
  ];
}

$db_conn->close();

// Return a server response
print json_encode($response);
