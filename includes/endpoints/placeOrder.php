<?php

// This file is called by front-end to place an order

session_start();

require_once('../config.php');
require_once(LIBRARY_PATH . '/db.php');
require_once(LIBRARY_PATH . '/formValidations.php');


header("Content-Type: application/json");

$response = [];

// Refuse GET requests
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  $response = [
    'status' => 'Method Not Allowed',
    'code' => 405
  ];
}

// If called with a post request, sanitize & validate incoming data and save the order
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['client_email_id'])) {

  $addressID = $db_conn->real_escape_string(trim(strtolower($_POST['addressID'])));

  try {
    $db_conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

    $qry = "INSERT INTO orders (email_id, address_id, order_date) 
        VALUES ('{$_SESSION['client_email_id']}', {$addressID}, NOW())";
    $db_conn->query($qry);
    $orderID = $db_conn->insert_id;


    foreach($_POST['items'] as $item) {

      // Pizza insert query
      if ($item['type'] === 'pizza') {

        $itemID = (int)$db_conn->real_escape_string($item['itemID']);
        $sauceID = (int)$db_conn->real_escape_string($item['sauce']);
        $sizeID = (int)$db_conn->real_escape_string($item['size']);
        $cheeseID = (int)$db_conn->real_escape_string($item['cheese']);
        $doughID = (int)$db_conn->real_escape_string($item['dough']);

        $qry = "INSERT INTO order_item (order_id, menu_item_id, sauce_id, size_id, cheese_id, dough_id) 
        VALUES ($orderID, $itemID, $sauceID, $sizeID, $cheeseID, $doughID)";
        $db_conn->query($qry);
        $orderItemID = $db_conn->insert_id;

        if (isset($item['toppings']) && count($item['toppings']) > 0) {
          foreach ($item['toppings'] as $key => $topping) {

            $toppingID = (int)$db_conn->real_escape_string($topping);

            $qry = "INSERT INTO order_item_topping (order_item_id, topping_id) 
          VALUES ($orderItemID, $toppingID)";
            $db_conn->query($qry);
          }
        }

        // Non-pizza item insert query
      } else {

        $itemID = $item['itemID'];

        $qry = "INSERT INTO order_item (order_id, menu_item_id) 
        VALUES ($orderID, $itemID)";
        $db_conn->query($qry);
      }
    }

    $db_conn->commit();

    $response = [
      'status' => 'OK',
      'code' => 200,
      'orderID' => $orderID
    ];

  } catch (Exception $e) {
    $db_conn->rollback();
    $response = [
      'status' => 'Error',
      'error' => $e
    ];
  }

}

$db_conn->close();

// Return a server response
print json_encode($response);