<?php

// Form validation code

function signupDataValid($email, $password) {
  $validationPassed = true;

  // Email validation
  if (strlen($email) > 100) {
    $validationPassed = false;
  } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $validationPassed = false;
  }

  // Password validations
  if (strlen($password) < 4) {
    $validationPassed = false;
  } else if (strlen($password) > 100) {
    $validationPassed = false;
  }

  return $validationPassed;
}


function loginDataValid($email) {
  $validationPassed = true;

  // Email validation
  if (strlen($email) > 100) {
    $validationPassed = false;
  } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $validationPassed = false;
  }

  return $validationPassed;
}


function addressValid($name, $phone, $street, $city, $province, $zipcode, $aptNum) {
  $validationPassed = true;

  // Name validations
  if (strlen($name) < 2) {
    $validationPassed = false;
  } else if (strlen($name) > 100) {
    $validationPassed = false;
  } else if (!preg_match('/^[a-z ,.\'-]+$/i', $name)) {
    $validationPassed = false;
  }

  // Phone validations
  if (strlen($phone) < 10) {
    $validationPassed = false;
  } else if (strlen($phone) > 20) {
    $validationPassed = false;
  } else if (!preg_match('/^(((\([0-9]{3}\))|([0-9]{3})) |[0-9]{3}-)[0-9]{3}-[0-9]{2}(-|)[0-9]{2}$/', $phone)) {
    $validationPassed = false;
  }

  // Street validations
  if (strlen($street) < 2) {
    $validationPassed = false;
  } else if (strlen($street) > 100) {
    $validationPassed = false;
  } else if (!preg_match('/^[a-z0-9 ,.\'-]+$/i', $street)) {
    $validationPassed = false;
  }

  // City validations
  if (strlen($city) < 2) {
    $validationPassed = false;
  } else if (strlen($city) > 100) {
    $validationPassed = false;
  } else if (!preg_match('/^[a-z ,.\'-]+$/i', $city)) {
    $validationPassed = false;
  }

  // Province validations
  if (!preg_match('/CA|AB|BC|MB|NL|NT|NU|PE|QC|SK|YT/', $province)) {
    $validationPassed = false;
  }

  // Zipcode validations
  if (!preg_match('/^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]( )?\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i', $zipcode)) {
    $validationPassed = false;
  }

  // Apartment number validations
  if ($aptNum !== '' && !preg_match('/[0-9]/', $aptNum)) {
      $validationPassed = false;
    }

  return $validationPassed;
}